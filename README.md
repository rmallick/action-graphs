### Overview
This is the Unofficial implementation of the code for the paper:

[Action Graphs: Weakly-supervised Action Localization with Graph Convolution Networks.](https://arxiv.org/abs/2002.01449) Maheen Rashid, Hedvig Kjellström, Yong Jae Lee. WACV 2020.


### Depencencies
You can set up the environments by using `$ pip3 install ...`.

- joblib==0.13.0
- numpy==1.14.5
- pandas==0.23.4
- scikit-learn==0.20.0
- scipy==1.1.0
- tensorboard==1.15.0
- tensorboard-logger==0.1.0
- tensorflow==1.15.2
- tensorflow-estimator==1.13.0
- torch>=1.0.0
- torchvision==0.2.1
- tqdm==4.31.1

### Data Preparation
1. Prepare [THUMOS'14](https://www.crcv.ucf.edu/THUMOS14/) dataset.
    - We excluded three test videos (270, 1292, 1496) as previous work did.

2. Extract features with two-stream I3D networks
    - We recommend extracting features using [this repo](https://github.com/piergiaj/pytorch-i3d).
    - For convenience, we provide the features we used. You can find them [here](https://drive.google.com/open?id=1NsVN2SPYEcS6sDnN4sfv2cAl0B0I8sp3).
    
3. Place the features inside the `dataset` folder.
    - Please ensure the data structure is as below.
   
~~~~
├── dataset
   └── THUMOS14
       ├── gt.json
       ├── split_train.txt
       ├── split_test.txt
       └── features
           ├── train
               ├── rgb
                   ├── video_validation_0000051.npy
                   ├── video_validation_0000052.npy
                   └── ...
               └── flow
                   ├── video_validation_0000051.npy
                   ├── video_validation_0000052.npy
                   └── ...
           └── test
               ├── rgb
                   ├── video_test_0000004.npy
                   ├── video_test_0000006.npy
                   └── ...
               └── flow
                   ├── video_test_0000004.npy
                   ├── video_test_0000006.npy
                   └── ...
~~~~

### Data Usage

The feature data of THUMOS 14 is directly used from the path /data/stars/user/rdai/PhD_work/BaSNet/dataset/THUMOS14
situated in the NEF Cluster


### Running
You can easily train and evaluate Action Graphs by running the script below.

If you want to try other training options, please refer to `options.py`.
The number of GPUs can be mentioned in the run script. The batch size can also be passed as an argument but here the batch size of 8 is used.
~~~~
$ bash run.sh
~~~~

### Evaulation
You can evaluate the model by running the command below.

~~~~
$ bash run_eval.sh
~~~~

For the original repository please refer to the following.

https://github.com/menorashid/action_graphs

### Reference 

1. https://github.com/Pilhyeon/BaSNet-pytorch