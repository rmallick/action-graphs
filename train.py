import torch
import torch.nn as nn
import numpy as np


class active_loss(nn.Module):
    def __init__(self, alpha):
        super(active_loss, self).__init__()
        self.alpha = alpha
        self.ce_criterion = nn.BCELoss()
        self.LogSoftmax = nn.LogSoftmax(dim=1)

    def forward(self, cos_sim_mat, score_fc, score_softmax, label):
        loss = {}
        label_normal = torch.cat((label, torch.ones((label.shape[0], 1)).cuda()), dim=1)
        label_normal = label_normal / torch.sum(label_normal, dim=1, keepdim=True)

        #loss_total = self.ce_criterion(score_fc, label_normal)
        #sparsity loss
        sparsity_loss = torch.sum(torch.abs(cos_sim_mat))    #/ 1048576
        sparsity_loss = sparsity_loss / (cos_sim_mat.size(0)*cos_sim_mat.size(1)) #1048576 #(cos_sim_mat[0].size()*cos_sim_mat[1].size())

        #loss_mil
        pred = self.LogSoftmax(score_fc)
        loss_mil = -1*label_normal*pred
        loss_mil = torch.sum(loss_mil, dim=1)
        loss_mil = torch.mean(loss_mil)
        print('sparsity_loss', sparsity_loss, 'mil_loss', loss_mil)
        loss_total = sparsity_loss + loss_mil
        loss["loss_total"] = loss_total

        return loss_total, loss




def train(net, train_loader, loader_iter, optimizer, criterion, logger, step):
    net.train()
    try:
        _data, _label, _, _, _ = next(loader_iter)
    except:
        loader_iter = iter(train_loader)
        _data, _label, _, _, _ = next(loader_iter)

    _data = _data.cuda()
    #print('train_data', _data, _data.size())
    _label = _label.cuda()
    #print('label_train_data', _label, _label.size())

    optimizer.zero_grad()

    cos_sim_mat, score_fc, score_softmax, _ = net(_data)
    #print('score_train', score_fc.size())
    cost, loss = criterion(cos_sim_mat, score_fc, score_softmax, _label)
    print(cost, loss)

    cost.backward()
    optimizer.step()

    for key in loss.keys():
        logger.log_value(key, loss[key].cpu().item(), step)
