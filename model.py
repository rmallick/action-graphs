import math

import torch
import torch.nn.functional as F
from torch import nn
from torch.nn.parameter import Parameter


class GraphConvolution(nn.Module):
    """
    Simple GCN layer, similar to https://arxiv.org/abs/1609.02907
    """

    def __init__(self, in_features, out_features, bias=True):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        print(self.weight.size())
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input, adj):

        #weight = torch.transpose(self.weight, 0, 1)
        #print('forward')
        #weight = torch.reshape(self.weight, (1,2048,20))
        #print('input_gcn', input.size(),  'weight', self.weight.size())
        support = torch.mm(input, self.weight)
        #print(support.size(), adj.size())
        output = torch.mm(adj, support)
        #print(output.size())
        # output = SparseMM(adj)(support)
        if self.bias is not None:
            return output + self.bias
        else:
            return output

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
               + str(self.in_features) + ' -> ' \
               + str(self.out_features) + ')'


class GCN(nn.Module):
    def __init__(self, len_feature, num_classes, dropout):
        super(GCN, self).__init__()
        self.len_feature = len_feature
        self.gc = GraphConvolution(len_feature, num_classes)
        self.dropout = dropout

    def forward(self, x, adj):
        x = F.relu(self.gc(x, adj))
        x = F.dropout(x, self.dropout, training=self.training)
        #x = self.gc(x, adj)
        return x


class active_graphs(torch.nn.Module):
    def __init__(self, len_feature, num_segments, test_mode=False):
        super(active_graphs, self).__init__()
        self.dropout = 0.8
        self.test_mode = test_mode
        self.act_feat_dim = len_feature
        self.num_class = 20
        self.l = 1024
        # self.comp_feat_dim = model_configs['comp_feat_dim']

        self.act_GCN = GCN(self.act_feat_dim, 1024, self.dropout)
        self.learned_phi = nn.Linear(self.l, self.l)
        self.activity_fc = nn.Linear(1024, self.num_class + 1)

        nn.init.normal_(self.activity_fc.weight.data, 0, 0.001)
        nn.init.constant_(self.activity_fc.bias.data, 0)

        self.softmax = nn.Softmax(dim=1)

        self.num_segments = num_segments
        self.k = num_segments // 8

    def forward(self, x):
        # construct feature matrix
        self.adj_num = 21
        self.x_feat_dim = 2048
        self.act_feat_dim = 1024
        self.comp_feat_dim = 1024
        self.child_num = 4

        #print('x', x.size())
        slices = torch.split(x, 1024, 2)
        x_i = slices[0]
        x_j = slices[1]
        print('x_i, x_j', x_i.size(), x_j.size())
        phi_i = self.learned_phi(x_i)
        phi_j = self.learned_phi(x_j)
        #print('phi', phi_i.size(), phi_j.size())
        # affinity_mat = F.cosine_similarity(phi_i, phi_j, dim=0)
        # print('affinity_mat', affinity_mat.size())

        x_ft_mat = x.view(-1, self.x_feat_dim).contiguous()
        row_ft_mat = phi_i.view(-1, self.act_feat_dim).contiguous()
        #print('row_mat_size', row_ft_mat.size())
        #print(x[0].size(), x[1].size(), row_ft_mat.size())
        col_ft_mat = phi_j.view(-1, self.comp_feat_dim).contiguous()
        #print('col_mat_size', col_ft_mat.size())

        # act cosine similarity
        dot_product_mat = torch.mm(row_ft_mat, torch.transpose(col_ft_mat, 0, 1))
        len_vec = torch.unsqueeze(torch.sqrt(torch.sum(row_ft_mat * col_ft_mat, dim=1)), dim=0)
        len_mat = torch.mm(torch.transpose(len_vec, 0, 1), len_vec)
        cos_sim_mat = dot_product_mat / len_mat
        #print(cos_sim_mat.size())

        # mask = row_ft_mat.new_zeros(21, 21)
        # for stage_cnt in range(self.child_num + 1):
        #     ind_list = list(range(1 + stage_cnt * self.child_num, 1 + (stage_cnt + 1) * self.child_num))
        #     for i, ind in enumerate(ind_list):
        #         mask[stage_cnt, ind] = 1 / self.child_num
        #     mask[stage_cnt, stage_cnt] = 1
        #
        # mask_mat_var = row_ft_mat.new_zeros(row_ft_mat.size()[0], row_ft_mat.size()[0])
        # for row in range(int(row_ft_mat.size(0) / self.adj_num)):
        #     mask_mat_var[row * self.adj_num: (row + 1) * self.adj_num, row * self.adj_num: (row + 1) * self.adj_num] \
        #         = mask

        adj = cos_sim_mat
        #adj = affinity_mat
        print('adjacency', adj)
        adj = F.relu(adj)
        out_gcn = self.act_GCN(x_ft_mat, adj)
        # output passed through the ReLU non
        out_gcn = F.relu(out_gcn)
        # L2 Normalize
        out_gcn = F.normalize(out_gcn, p=2)
        out_gcn_dropout = F.dropout(out_gcn, p=0.5)

        fully_connected = self.activity_fc(out_gcn_dropout)
        fully_connected = F.tanh(fully_connected)
        #print('fully_connected_tanh', fully_connected)
        fully_connected = fully_connected.view(8, 1024, self.num_class + 1)
        #print('fully_connected', fully_connected.size())

        score_fc = torch.mean(torch.topk(fully_connected, self.k, dim=1)[0], dim=1)
        #print('score_fc', score_fc, score_fc.size(), torch.topk(fully_connected, self.k, dim=1)[0].size())
        score_softmax = self.softmax(score_fc)
        #print('score_fc', score_fc, score_fc.size())


        return cos_sim_mat, score_fc, score_softmax, fully_connected



class active_graphs_test(torch.nn.Module):
    def __init__(self, len_feature, num_segments, test_mode=False):
        super(active_graphs_test, self).__init__()
        self.dropout = 0.8
        self.test_mode = test_mode
        self.act_feat_dim = len_feature
        self.num_class = 20
        self.l = 1024
        # self.comp_feat_dim = model_configs['comp_feat_dim']

        self.act_GCN = GCN(self.act_feat_dim, 1024, self.dropout)
        self.learned_phi = nn.Linear(self.l, self.l)
        self.activity_fc = nn.Linear(1024, self.num_class + 1)
        self.cos = nn.CosineSimilarity(dim=1, eps=1e-6)

        nn.init.normal_(self.activity_fc.weight.data, 0, 0.001)
        nn.init.constant_(self.activity_fc.bias.data, 0)

        self.softmax = nn.Softmax(dim=1)

        self.num_segments = num_segments
        self.k = num_segments // 8

    def forward(self, x):
        # construct feature matrix
        self.adj_num = 21
        self.act_feat_dim = 1024
        self.comp_feat_dim = 1024
        self.x_feat_dim = 2048
        self.child_num = 4

        #print('x', x.size())
        slices = torch.split(x, 1024, 2)
        x_i = slices[0]
        x_j = slices[1]
        #print('x_i, x_j', x_i.size(), x_j.size())
        phi_i = self.learned_phi(x_i)
        phi_j = self.learned_phi(x_j)
        #print('phi', phi_i.size(), phi_j.size())
        # affinity_mat = F.cosine_similarity(phi_i, phi_j, dim=0)
        # print('affinity_mat', affinity_mat.size())


        x_ft_mat = x.view(-1, self.x_feat_dim).contiguous()
        row_ft_mat = phi_i.view(-1, self.act_feat_dim).contiguous()
        #print('row_mat_size', row_ft_mat.size())
        #print(x[0].size(), x[1].size(), row_ft_mat.size())
        col_ft_mat = phi_j.view(-1, self.comp_feat_dim).contiguous()
        #print('col_mat_size', col_ft_mat.size())

        #act cosine similarity
        dot_product_mat = torch.mm(row_ft_mat, torch.transpose(col_ft_mat, 0, 1))
        len_vec = torch.unsqueeze(torch.sqrt(torch.sum(row_ft_mat * col_ft_mat, dim=1)), dim=0)
        len_mat = torch.mm(torch.transpose(len_vec, 0, 1), len_vec)
        cos_sim_mat = dot_product_mat / len_mat
        #print(cos_sim_mat.size())

        # mask = row_ft_mat.new_zeros(21, 21)
        # for stage_cnt in range(self.child_num + 1):
        #     ind_list = list(range(1 + stage_cnt * self.child_num, 1 + (stage_cnt + 1) * self.child_num))
        #     for i, ind in enumerate(ind_list):
        #         mask[stage_cnt, ind] = 1 / self.child_num
        #     mask[stage_cnt, stage_cnt] = 1
        #
        # mask_mat_var = row_ft_mat.new_zeros(row_ft_mat.size()[0], row_ft_mat.size()[0])
        # for row in range(int(row_ft_mat.size(0) / self.adj_num)):
        #     mask_mat_var[row * self.adj_num: (row + 1) * self.adj_num, row * self.adj_num: (row + 1) * self.adj_num] \
        #         = mask

        adj =  cos_sim_mat
        #adj = affinity_mat
        print('adjacency', adj.size())
        adj = F.relu(adj)
        out_gcn = self.act_GCN(x_ft_mat, adj)
        # output passed through the ReLU non
        out_gcn = F.relu(out_gcn)
        # L2 Normalize
        out_gcn = F.normalize(out_gcn, p=2)
        out_gcn_dropout = F.dropout(out_gcn, p=0.5)

        fully_connected = self.activity_fc(out_gcn_dropout)
        fully_connected = F.tanh(fully_connected)
        #print('fully_connected_tanh', fully_connected)
        fully_connected = fully_connected.view(1, 1024, self.num_class + 1)
        #print('fully_connected', fully_connected.size())

        score_fc = torch.mean(torch.topk(fully_connected, self.k, dim=1)[0], dim=1)
        #print('score_fc', score_fc, score_fc.size(), torch.topk(fully_connected, self.k, dim=1)[0].size())
        score_softmax = self.softmax(score_fc)
        #print('score_fc', score_fc, score_fc.size())


        return cos_sim_mat, score_fc, score_softmax, fully_connected
