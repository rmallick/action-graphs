import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as

def MILLoss(self, X, Y):
    Y = Y.float()
    Y_prob, _, A = self.forward(X)
    Y_prob = torch.clamp(Y_prob, min=-0.5, max=0.5)


